## Test Lider Tecnico - CleverIT

El siguiente documento tiene información util sobre el proyecto desarrollado para la prueba tecnica.

El proyecto se divide en sus 5 puntos donde los checkpoint asociados son:

```bash
Punto 1 - Resuelto [a28e9ed22dca98a55261e237d486304ed9d7a9aa]
Punto 2 - Resuelto [df7a3b49876f4bb424d35782ff14d121bbfbba36]
Punto 3 - Resuelto [a973d9f14876182f54069f962d883a7e04df538b]
Punto 4 - Resuelto [a7eecddc817940bb0abf423da6054aebbf06bafb]
Punto 5 - Resuelto [0963564980a2fb70e4ebebeac8ec4819a3483e9e]
```

Para vialualizarlos haga uso de git para moverse en la linea historica del proyecto.

### Arquitectura

- La aplicación esta compuesta de un backend escrito en JAVA Spring, el cual sirve un frontend embeido escrito en React.js + Redux

- En una primer instancia el servidor renderiza los datos que lee react (Punto 1) y es capaz de ejecutar las operaciones de un CRUD usando URL que ejecutan operaciones y recargan la información.

- El problema 2 fue desarrollado haciendo uso de MYSQL, el cual se encuentra dockerizado para entorno de desarrollo, junto a un phpmyadmin para utilizarlo. El endpoint quedo ubicado en (Por alguna razon la llame durante todo el test cars, por lo cual deje las entidades y la api asi xD):

```bash
http://{IP_HOSTED}/hooks/cars
```

- El problema 3 uso el frontend desarrollado y se le incorporo Axios para gestionar las llamadas HTTP AJAX y se incremento la composición del nucleo en REDUX, para gestionar las acciones asincronas.

- Para el problema 4 la aplicacion fue dockerizada con una fase de construcción multistage, de forma de finalmente dejar un .jar limpio de bajo tamaño y libre de archivos de desarrollo. Se dejo disponible un docker-compose para su despliegue IN-HOUSE

- Se utilizo GCloud + Kubernetes + Cloud SQL para desplegar la app disponible en:

```curl
http://34.66.133.88/
```

- Los archivos de configuración utilizados para el despliegue quedaron versionados en las carpetas .docker y .kubernetes

### Dependencias

- Docker
- Nodejs, ademas un gestionador de paquetes (NPM o Yarn)
- Java 8
- Maven

### Instrucciones de compilación y uso IN-HOUSE

La aplicación se encuentra dockerizada, y ademas cuenta con sus archivos docker-compose, por lo que basta con escribir:

```bash
docker-compose up -d
````

Con esta la aplicación quedara lista para ser usada desde el puerto 80 en:

```curl
http://localhost
```

### Compilar frontend

Para compilar el frontend acceda a la capreta ubicada en resources y escriba el siguiente comando:

```bash
npm run build
````

Esto compilara la aplicación y la ubicara en el directorio main/resources/static del proyecto JAVA

### Compilar el backend

Asegurese tener compilado en frontend y luego escriba el siguiente comando.

```bash
mvn clean package spring-boot:repackage
```

Con lo cual el paquete .Jar quedara ubicado en la carpeta target del proyecto.


## Que disfrutes :)