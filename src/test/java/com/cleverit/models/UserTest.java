package com.cleverit.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonObject;

public class UserTest {
  private User user;
  
  @Before
  public void setup() {
    user = new User("76u4jh", "Pedro", "Perico", "Ingeniero", "pedro@gmail.com");
  }
  
  @Test
  public void testInizialitation() {
    user = new User();
    assertEquals(this.user.getId(), null);
    assertEquals(this.user.getName(), null);
    assertEquals(this.user.getLastname(), null);
    assertEquals(this.user.getProfesion(), null);
    assertEquals(this.user.getEmail(), null);
  }
  
  @Test
  public void testInizialitationWithArgs() {
    user = new User("76u4jh", "Carlos", "Genial", "medico", "carlos@gmail.com");
    assertEquals(this.user.getId(), "76u4jh");
    assertEquals(this.user.getName(), "Carlos");
    assertEquals(this.user.getLastname(), "Genial");
    assertEquals(this.user.getProfesion(), "medico");
    assertEquals(this.user.getEmail(), "carlos@gmail.com");
  }
  
  @Test
  public void testBuilder() {
    user = User.builder()
        .id("123456")
        .name("Carlos")
        .lastname("Pesimo")
        .profesion("Bandalo")
        .email("carlos@gmail.com")
        .build();
    
    assertEquals(this.user.getId(), "123456");
    assertEquals(this.user.getName(), "Carlos");
    assertEquals(this.user.getLastname(), "Pesimo");
    assertEquals(this.user.getProfesion(), "Bandalo");
    assertEquals(this.user.getEmail(), "carlos@gmail.com");
  }
  
  @Test
  public void testBuilderToString() {
    String user = User.builder()
        .id("123456")
        .name("Carlos")
        .lastname("Pesimo")
        .profesion("Bandalo")
        .email("carlos@gmail.com")
        .toString();
    assertNotNull(user);
  }
  
  
  @Test
  public void testGetters() {
    assertEquals(this.user.getId(), "76u4jh");
    assertEquals(this.user.getName(), "Pedro");
    assertEquals(this.user.getLastname(), "Perico");
    assertEquals(this.user.getProfesion(), "Ingeniero");
    assertEquals(this.user.getEmail(), "pedro@gmail.com");
  }
  
  @Test
  public void testSetters() {
    this.user.setId("pppppp");
    assertEquals(this.user.getId(), "pppppp");
    this.user.setName("Jirafa");
    assertEquals(this.user.getName(), "Jirafa");
    this.user.setLastname("Animalista");
    assertEquals(this.user.getLastname(), "Animalista");
    this.user.setProfesion("Activista");
    assertEquals(this.user.getProfesion(), "Activista");
    this.user.setEmail("jirafa@hotmail.com");
    assertEquals(this.user.getEmail(), "jirafa@hotmail.com");
  }
  
  @Test
  public void testGetJSON() {
    JsonObject json = this.user.toJSON();
    assertEquals(json.get("id"), null);
    assertEquals(json.get("nombre").getAsString(), "Pedro");
    assertEquals(json.get("apellido").getAsString(), "Perico");
    assertEquals(json.get("profesion").getAsString(), "Ingeniero");
    assertEquals(json.get("email").getAsString(), "pedro@gmail.com");
  }
}
