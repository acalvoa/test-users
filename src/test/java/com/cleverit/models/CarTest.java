package com.cleverit.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class CarTest {
  private Car car;
  
  @Before
  public void setup() {
    car = new Car("76u4jh", "HJ5678", "Camion", "Rojo");
  }
  
  @Test
  public void testInizialitation() {
    car = new Car();
    assertEquals(this.car.getId(), null);
    assertEquals(this.car.getPlate(), null);
    assertEquals(this.car.getColor(), null);
    assertEquals(this.car.getType(), null);
  }
  
  @Test
  public void testInizialitationWithArgs() {
    car = new Car("123456", "HJ5695", "auto", "Amarillo");
    assertEquals(this.car.getId(), "123456");
    assertEquals(this.car.getPlate(), "HJ5695");
    assertEquals(this.car.getColor(), "Amarillo");
    assertEquals(this.car.getType(), "auto");
  }
  
  @Test
  public void testBuilder() {
    car = Car.builder()
        .id("123456")
        .plate("HJ5695")
        .type("auto")
        .color("Amarillo")
        .build();
    
    assertEquals(this.car.getId(), "123456");
    assertEquals(this.car.getPlate(), "HJ5695");
    assertEquals(this.car.getColor(), "Amarillo");
    assertEquals(this.car.getType(), "auto");
  }
  
  @Test
  public void testBuilderToString() {
    String car = Car.builder()
        .id("123456")
        .plate("HJ5695")
        .type("auto")
        .color("Amarillo")
        .toString();
    
    assertNotNull(car);
  }
  
  
  @Test
  public void testGetters() {
    assertEquals(this.car.getId(), "76u4jh");
    assertEquals(this.car.getPlate(), "HJ5678");
    assertEquals(this.car.getColor(), "Rojo");
    assertEquals(this.car.getType(), "Camion");
  }
  
  @Test
  public void testSetters() {
    this.car.setId("pppppp");
    assertEquals(this.car.getId(), "pppppp");
    this.car.setPlate("JBFT51");
    assertEquals(this.car.getPlate(), "JBFT51");
    this.car.setColor("Azul");
    assertEquals(this.car.getColor(), "Azul");
    this.car.setType("Azul");
    assertEquals(this.car.getType(), "Azul");
  }
}
