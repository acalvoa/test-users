package com.cleverit.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.cleverit.services.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserRestControllerTest {
  @Mock
  private UserService restClientService;
  
  private UserRestController controller;
  
  private List<User> users; 
  
  @Before
  public void setup() throws HttpErrorException, JSONParsingException {
    MockitoAnnotations.initMocks(this);

    controller = new UserRestController();
    ReflectionTestUtils.setField(controller, "restClientService", restClientService);
    
    this.users = new ArrayList<User>();
    this.users.add(new User());
    
    when(restClientService.fetch()).thenReturn(users);
    
    when(restClientService.create(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
      .thenAnswer(invocation -> {
        User user = new User("id", 
            (String) invocation.getArguments()[0],
            (String) invocation.getArguments()[1],
            (String) invocation.getArguments()[2],
            (String) invocation.getArguments()[3]);
        
        users.add(user);
        return user;
      });
    
    doAnswer(invocation -> {
      User user = User.builder()
          .id((String)invocation.getArguments()[0])
          .name((String)invocation.getArguments()[1])
          .lastname((String)invocation.getArguments()[2])
          .profesion((String)invocation.getArguments()[3])
          .email((String)invocation.getArguments()[4])
          .build();
      users.set(0, user);
      return null;
    })
    .when(restClientService).update(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
      
    doAnswer(invocation -> {
      users.remove(0);
      return null;
    })
    .when(restClientService).delete(Mockito.anyString());
  }
  
  @Test
  public void testCreate() throws HttpErrorException, JSONParsingException {
    assertEquals(this.users.size(), 1);
    User user = User.builder()
        .name("test")
        .lastname("test2")
        .profesion("ingenierio")
        .email("test@gmail.com")
        .build();
    
    this.controller.create(user);
    assertEquals(this.users.size(), 2);
  }
  
  @Test
  public void testFetch() throws HttpErrorException, JSONParsingException {
    users.add(new User());
    String result = this.controller.fetch();
    Gson gson = new Gson();
    Type type = new TypeToken<List<User>>() {}.getType();
    List<User> json = gson.fromJson(result, type);
    assertEquals(json.size(), 2);
  }
  
  @Test
  public void testUpdate() throws HttpErrorException, JSONParsingException {
    User user = User.builder()
        .name("test")
        .lastname("test2")
        .profesion("ingenierio")
        .email("test@gmail.com")
        .build();
        
    this.controller.update("iidd", user);
    
    User result = this.users.get(0);
    assertEquals(result.getId(), "iidd");
    assertEquals(result.getName(), "test");
    assertEquals(result.getLastname(), "test2");
    assertEquals(result.getProfesion(), "ingenierio");
    assertEquals(result.getEmail(), "test@gmail.com");
  }
  
  @Test
  public void testDelete() throws HttpErrorException, JSONParsingException {
    assertEquals(this.users.size(), 1);
    this.controller.delete("iidd");
    assertEquals(this.users.size(), 0);
  }
  
  @Test
  public void testHttpError() {
    ResponseEntity<String> response = controller.handleHttpException(new HttpErrorException(404));
    assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    assertEquals(response.getBody(), "");
  }
  
  @Test
  public void testJSONError() {
    ResponseEntity<String> response = controller.handleJSONException();
    assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    assertEquals(response.getBody(), "");
  }
}
