package com.cleverit.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.ModelAndView;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.cleverit.services.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserControllerTest {
  @Mock
  private UserService restClientService;
  
  private UserController controller;
  
  private List<User> users; 
  
  @Before
  public void setup() throws HttpErrorException, JSONParsingException {
    MockitoAnnotations.initMocks(this);

    controller = new UserController();
    ReflectionTestUtils.setField(controller, "restClientService", restClientService);
    
    this.users = new ArrayList<User>();
    this.users.add(new User());
    
    when(restClientService.fetch()).thenReturn(this.users);
    
    when(restClientService.createAndFetch(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
      .thenAnswer(invocation -> {
        User user = new User("id", 
            (String) invocation.getArguments()[0],
            (String) invocation.getArguments()[1],
            (String) invocation.getArguments()[2],
            (String) invocation.getArguments()[3]);
        
        users.add(user);
        return users;
      });
    
    doAnswer(invocation -> {
      User user = User.builder()
          .id((String)invocation.getArguments()[0])
          .name((String)invocation.getArguments()[1])
          .lastname((String)invocation.getArguments()[2])
          .profesion((String)invocation.getArguments()[3])
          .email((String)invocation.getArguments()[4])
          .build();
      users.set(0, user);
      return users;
    })
    .when(restClientService).updateAndFetch(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
      
    doAnswer(invocation -> {
      users.remove(0);
      return users;
    })
    .when(restClientService).deleteAndFetch(Mockito.anyString());
  }
  
  @Test
  public void testCreate() throws HttpErrorException, JSONParsingException {
    assertEquals(this.users.size(), 1);
    ModelAndView result = this.controller.create(new HashMap<String, Object>(), "Max", "Powel", "Agente", "max@gmail.com");
    String content = (String) result.getModel().get("content");
    System.out.println(content);
    Gson gson = new Gson();
    Type type = new TypeToken<List<User>>() {}.getType();
    List<User> json = gson.fromJson(content, type);
    
    assertEquals(json.size(), 2);
  }
  
  @Test
  public void testFetch() throws HttpErrorException, JSONParsingException {
    assertEquals(this.users.size(), 1);
    ModelAndView result = this.controller.fetch(new HashMap<String, Object>());
    String content = (String) result.getModel().get("content");
    
    Gson gson = new Gson();
    Type type = new TypeToken<List<User>>() {}.getType();
    List<User> json = gson.fromJson(content, type);
    
    assertEquals(json.size(), 1);
  }
  
  @Test
  public void testUpdate() throws HttpErrorException, JSONParsingException {
    assertEquals(this.users.size(), 1);
    ModelAndView result = this.controller.update(new HashMap<String, Object>(), "iidd", "test", "test2", "ingenierio", "test@gmail.com");
    String content = (String) result.getModel().get("content");
    
    Gson gson = new Gson();
    Type type = new TypeToken<List<User>>() {}.getType();
    List<User> json = gson.fromJson(content, type);
    
    assertEquals(json.size(), 1);
    
    User resultchanged = json.get(0);
    assertEquals(resultchanged.getId(), "iidd");
    assertEquals(resultchanged.getName(), "test");
    assertEquals(resultchanged.getLastname(), "test2");
    assertEquals(resultchanged.getProfesion(), "ingenierio");
    assertEquals(resultchanged.getEmail(), "test@gmail.com");
  }
  
  @Test
  public void testDelete() throws HttpErrorException, JSONParsingException {
    assertEquals(this.users.size(), 1);
    ModelAndView result = this.controller.delete(new HashMap<String, Object>(), "iidd");
    String content = (String) result.getModel().get("content");
    
    Gson gson = new Gson();
    Type type = new TypeToken<List<User>>() {}.getType();
    List<User> json = gson.fromJson(content, type);
    
    assertEquals(json.size(), 0);
  }
  
  @Test
  public void testHttpError() {
    ModelAndView response = controller.handleHttpException(new HttpErrorException(404));
    String content = (String) response.getModel().get("content");
    int code = (int) response.getModel().get("error");
    assertEquals(content, "[]");
    assertEquals(code, 404);
  }
  
  @Test
  public void testJSONError() {
    ModelAndView response = controller.handleJSONException();
    String content = (String) response.getModel().get("content");
    String code = (String) response.getModel().get("error");
    assertEquals(content, "[]");
    assertEquals(code, "JSON_PARSING");
  }
}
