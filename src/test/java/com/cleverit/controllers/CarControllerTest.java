package com.cleverit.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.Car;

import com.cleverit.services.CarService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

public class CarControllerTest {
  @Mock
  private CarService carService;
  
  private CarController controller;
  
  private List<Car> cars;
  
  @Before
  public void setup() throws JSONParsingException, HttpErrorException {
    MockitoAnnotations.initMocks(this);
    
    controller = new CarController();
    ReflectionTestUtils.setField(controller, "carService", carService);
    
    cars = new ArrayList<Car>();
    cars.add(new Car());
    
    when(carService.fetchFromApiAndSave()).thenReturn(this.cars);
  }
  
  @Test
  public void testFetchAndSave() throws JSONParsingException, HttpErrorException {
    String result = controller.fetch();
    assertNotNull(result);
    
    Gson gson = new Gson();
    Type type = new TypeToken<List<Car>>() {}.getType();
    List<Car> json = gson.fromJson(result, type);
    assertEquals(json.size(), 1);
  }
  
  @Test(expected = HttpErrorException.class)
  public void testFetchAndSaveFail() throws JSONParsingException, HttpErrorException {
    when(carService.fetchFromApiAndSave()).thenThrow(HttpErrorException.class);
    controller.fetch();
  }
  
  @Test(expected = JSONParsingException.class)
  public void testFetchAndSaveFailJSON() throws JSONParsingException, HttpErrorException {
    when(carService.fetchFromApiAndSave()).thenThrow(JSONParsingException.class);
    controller.fetch();
  }
}
