package com.cleverit.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.cleverit.repositories.UserApiRepository;

public class UserServiceTest {
  
  @Mock
  private UserApiRepository userApiRepository;
  
  private List<User> users;
  private UserService userService;
  
  @Before
  public void setup() throws JSONParsingException, HttpErrorException {
    MockitoAnnotations.initMocks(this);
    this.users = new ArrayList<User>();
    
    userService = new UserService();
    ReflectionTestUtils.setField(userService, "userRepository", userApiRepository);
    
    when(userApiRepository.fetch()).thenReturn(users);
    when(userApiRepository.create(Mockito.any(User.class))).thenAnswer(invocation -> {
      users.add((User) invocation.getArguments()[0]);
      return invocation.getArguments()[0];
    });

    doNothing().when(userApiRepository).update(Mockito.anyString(), Mockito.any(User.class));
    
    doAnswer(invocation -> {
      users.remove(0);
      return null;
    }).when(userApiRepository).delete(Mockito.anyString());
  }
  
  @Test
  public void testZeroUser() throws JSONParsingException, HttpErrorException {
    assertEquals(this.userService.fetch().size(), 0);
  }
  
  @Test
  public void testTwoUser() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 2);
  }
  
  @Test
  public void createUserTest() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 1);
    this.userService.create("Test", "Test", "Test", "Test");
    this.userService.create("Test", "Test", "Test", "Test");
    assertEquals(this.userService.fetch().size(), 3);
  }
  
  @Test
  public void createUserAndFetchTest() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 2);
    assertEquals(this.userService.createAndFetch("Test", "Test", "Test", "Test").size(), 3);
  }
  
  @Test
  public void UpdateUserTest() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 1);
    this.userService.update("sfsd", "Test", "Test", "Test", "Test");
    assertEquals(this.userService.fetch().size(), 1);
  }
  
  @Test
  public void UpdateUserAndFetchTest() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 2);
    assertEquals(this.userService.updateAndFetch("sfsd", "Test", "Test", "Test", "Test").size(), 2);
  }
  
  @Test
  public void deleteUserTest() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 1);
    this.userService.delete("Test");
    assertEquals(this.userService.fetch().size(), 0);
  }
  
  @Test
  public void deleteUserAndFetchTest() throws JSONParsingException, HttpErrorException {
    users.add(new User());
    users.add(new User());
    assertEquals(this.userService.fetch().size(), 2);
    assertEquals(this.userService.deleteAndFetch("Test").size(), 1);
  }
  
}
