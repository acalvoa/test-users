package com.cleverit.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.Car;
import com.cleverit.repositories.CarsApiRepository;
import com.cleverit.repositories.CarsRepository;

public class CarServiceTest {
  
  @Mock
  private CarsApiRepository carsApiRepository;
  
  @Mock
  private CarsRepository carsRepository;
  
  private List<Car> cars;
  private List<Car> db;
  
  private CarService carService;
  
  @Before
  public void setup() throws JSONParsingException, HttpErrorException {
    MockitoAnnotations.initMocks(this);
    
    carService = new CarService();
    ReflectionTestUtils.setField(carService, "carsApiRepository", carsApiRepository);
    ReflectionTestUtils.setField(carService, "carsRepository", carsRepository);
    
    this.cars = new ArrayList<Car>();
    this.db = new ArrayList<Car>();
    
    when(carsApiRepository.fetch()).thenReturn(cars);
    
    doAnswer(invocation -> {
      db.add((Car) invocation.getArguments()[0]);
      return invocation.getArguments()[0];
    }).when(carsRepository).save(Mockito.any(Car.class));
    
    
  }
  
  @Test
  public void testOneCarCopy() throws JSONParsingException, HttpErrorException {
    cars.add(new Car());
    assertEquals(this.carService.fetchFromApiAndSave().size(), 1);
  }
  
  @Test
  public void testTwoCarCopy() throws JSONParsingException, HttpErrorException {
    cars.add(new Car());
    cars.add(new Car());
    assertEquals(this.carService.fetchFromApiAndSave(), cars);
  }
  
  @Test
  public void testZeroCarCopy() throws JSONParsingException, HttpErrorException {
    assertEquals(this.carService.fetchFromApiAndSave(), cars);
  }
}
