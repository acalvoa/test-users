package com.cleverit.exceptions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class JSONParsingExceptionTest {
    
  @Test(expected = JSONParsingException.class)
  public void testException() throws JSONParsingException {
    JSONParsingException exception = new JSONParsingException();
    assertEquals(exception.getMessage(), null);
    throw exception;
  }
}
