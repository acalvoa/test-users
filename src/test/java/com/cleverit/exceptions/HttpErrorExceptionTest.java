package com.cleverit.exceptions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HttpErrorExceptionTest {
  
  @Test(expected = HttpErrorException.class)
  public void testException() throws HttpErrorException {
    HttpErrorException exception = new HttpErrorException(404);
    assertEquals(exception.getCode(), 404);
    assertEquals(exception.getMessage(), null);
    throw exception;
  }
}
