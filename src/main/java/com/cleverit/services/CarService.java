package com.cleverit.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.Car;
import com.cleverit.repositories.CarsApiRepository;
import com.cleverit.repositories.CarsRepository;

@Service
public class CarService {
  
  @Autowired
  private CarsApiRepository carsApiRepository;
  
  @Autowired
  private CarsRepository carsRepository;
  
  public List<Car> fetchFromApiAndSave() throws JSONParsingException, HttpErrorException {
    List<Car> cars = this.carsApiRepository.fetch();
    cars.forEach(this.carsRepository::save);
    return cars;
  }
}
