package com.cleverit.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.cleverit.repositories.UserApiRepository;

@Service
public class UserService {
  
	@Autowired
	private UserApiRepository userRepository;
	
	public List<User> fetch() throws HttpErrorException, JSONParsingException {
	  return this.userRepository.fetch();
	}
	
	public List<User> createAndFetch(String name, String lastname, String profesion, String email) 
	    throws HttpErrorException, JSONParsingException {
		this.create(name, lastname, profesion, email);
		return this.userRepository.fetch();
	}
	
	public User create(String name, String lastname, String profesion, String email) 
      throws HttpErrorException, JSONParsingException {
    User user = User.builder()
                  .name(name)
                  .lastname(lastname)
                  .profesion(profesion)
                  .email(email)
                  .build();
    return userRepository.create(user);
  }
	
	public List<User> updateAndFetch(String id, String name, String lastname, String profesion, String email) 
	    throws HttpErrorException, JSONParsingException {
	  this.update(id, name, lastname, profesion, email);
    return this.userRepository.fetch();
	}
	
	public void update(String id, String name, String lastname, String profesion, String email) 
      throws HttpErrorException, JSONParsingException {
    User user = User.builder()
        .name(name)
        .lastname(lastname)
        .profesion(profesion)
        .email(email)
        .build();
    userRepository.update(id, user);
  }
	
	public List<User> deleteAndFetch(String id) throws HttpErrorException, JSONParsingException {
	  this.delete(id);
	  return this.userRepository.fetch();
	}
	
	public void delete(String id) throws HttpErrorException, JSONParsingException {
    this.userRepository.delete(id);
  }
}
