package com.cleverit.repositories;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.Car;
import com.google.api.client.http.HttpResponse;
import com.google.gson.reflect.TypeToken;

@Repository
public class CarsApiRepository extends RestRepository {
  
  @Value("${com.cleverit.user.api}")
  private String uri;
  
  public List<Car> fetch() throws JSONParsingException, HttpErrorException {
    try {
      HttpResponse request = this.Get(uri);
      Type type = new TypeToken<List<Car>>() {}.getType();
      return (List<Car>) request.parseAs(type);
    } catch (IOException | IllegalArgumentException e) {
      throw new JSONParsingException();
    }
  }
}
