package com.cleverit.repositories;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpResponse;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;


@Repository
public class UserApiRepository extends RestRepository {
  
  @Value("${com.cleverit.user.api}")
  private String uri;

	public List<User> fetch() throws HttpErrorException, JSONParsingException {
    try {
      HttpResponse request = this.Get(uri);
      Type type = new TypeToken<List<User>>() {}.getType();
      return (List<User>) request.parseAs(type);
    } catch (IOException | IllegalArgumentException e) {
      throw new JSONParsingException();
    }
	}
	
	public User create(User user) throws HttpErrorException, JSONParsingException {
	  try {
	    HttpContent content = ByteArrayContent.fromString("application/json", user.toJSON().toString());
	    HttpResponse request = this.Post(uri, content);
	    Type type = new TypeToken<User>() {}.getType();
	    return (User) request.parseAs(type);
	  } catch(IOException | IllegalArgumentException e) {
	    throw new JSONParsingException();
	  }
	}
	
	public void update(String id, User user) throws HttpErrorException {
	  HttpContent content = ByteArrayContent.fromString("application/json", user.toJSON().toString());
    this.Put(String.format("%s/%s", uri, id), content);
	}
	
	public void delete(String id) throws HttpErrorException {
	  this.Delete(String.format("%s/%s", uri, id));
	}
}
