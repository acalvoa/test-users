package com.cleverit.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import com.cleverit.models.Car;

@Repository
public interface CarsRepository extends CrudRepository<Car, Integer> {
}
