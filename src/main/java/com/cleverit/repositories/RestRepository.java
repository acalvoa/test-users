package com.cleverit.repositories;

import java.io.IOException;

import com.cleverit.exceptions.HttpErrorException;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

public class RestRepository {
  private HttpTransport HttpTransport = new NetHttpTransport();
  private JsonFactory jsonParser = new JacksonFactory();
  
  protected HttpResponse Get(String uri) throws HttpErrorException {
    try {
      HttpRequestFactory requestFactory = HttpTransport
          .createRequestFactory(request -> {
            request.setParser(new JsonObjectParser(jsonParser));
          });
      
      return requestFactory
          .buildGetRequest(new GenericUrl(uri))
          .execute();
    } catch (IOException e) {
      HttpResponseException ex = (HttpResponseException) e;
      throw new HttpErrorException(ex.getStatusCode());
    }
  }
  
  protected HttpResponse Post(String uri, HttpContent content) throws HttpErrorException {
    try {
      HttpRequestFactory requestFactory = HttpTransport
          .createRequestFactory(request -> {
            request.setParser(new JsonObjectParser(jsonParser));
          });
      
      HttpRequest request = requestFactory
          .buildPostRequest(new GenericUrl(uri), content);
      
      request.getHeaders().setContentType("application/json");
      
      return request.execute();
    } catch (IOException e) {
      HttpResponseException ex = (HttpResponseException) e;
      throw new HttpErrorException(ex.getStatusCode());
    }
  }
  
  protected HttpResponse Put(String uri, HttpContent content) throws HttpErrorException {
    try {
      HttpRequestFactory requestFactory = HttpTransport
          .createRequestFactory(request -> {
            request.setParser(new JsonObjectParser(jsonParser));
          });
      
      HttpRequest request = requestFactory
          .buildPutRequest(new GenericUrl(uri), content);
      
      request.getHeaders().setContentType("application/json");
      
      return request.execute();
    } catch (IOException e) {
      HttpResponseException ex = (HttpResponseException) e;
      throw new HttpErrorException(ex.getStatusCode());
    }
  }
  
  protected HttpResponse Delete(String uri) throws HttpErrorException {
    try {
      HttpRequestFactory requestFactory = HttpTransport
          .createRequestFactory();
      
      return requestFactory
          .buildDeleteRequest(new GenericUrl(uri))
          .execute();
    } catch (IOException e) {
      HttpResponseException ex = (HttpResponseException) e;
      throw new HttpErrorException(ex.getStatusCode());
    }
  }
}
