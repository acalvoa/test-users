package com.cleverit.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.Car;
import com.cleverit.services.CarService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/hooks/cars")
public class CarController {

  @Autowired
  private CarService carService;
  /**
   * Este metodo se encarga de a traves del servicio gestionado ir a buscar la información a la API
   * remota, la cual es transformada temporalmente en un List<Car> y almacenada en una base de datos
   * MYSQL 5.7. Este metodo devuelve un JSON con todo el payload obtenido en la API.
   * 
   * @author acalvoa
   * 
   * @return {String} Retorna el payload que la API de patentes dio origen.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public String fetch() throws JSONParsingException, HttpErrorException {
    List<Car> cars = this.carService.fetchFromApiAndSave();
    Gson gson = new Gson();
    return gson.toJson(cars);
  }
  
}
