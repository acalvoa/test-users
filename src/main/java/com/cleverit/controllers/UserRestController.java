package com.cleverit.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.cleverit.services.UserService;
import com.google.gson.Gson;

@RestController
@RequestMapping(path = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {
  
  @Autowired
  private UserService restClientService;
  
  /**
   * Este metodo se utiliza para obtener un array JSON con los usuarios dentro de la API.
   * 
   * @author acalvoa
   * 
   * @return Devuelve una Array JSON con los usuarios obtenidos por la API.
   */
  @GetMapping
  public String fetch() 
      throws HttpErrorException, JSONParsingException {
    List<User> users = this.restClientService.fetch();
    Gson gson = new Gson();
    return gson.toJson(users);
  }
  
  /**
   * Este metodo se utiliza para crear un usuario. La data se gestiona en formato JSON encode.
   * 
   * @author acalvoa
   * 
   * @param {User} user       El objeto usuario con los datos proveidos por el JSON en la llamada.
   * 
   * @return Devuelve el usuario creado, en formato JSON.
   */
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(code = HttpStatus.CREATED)
  public String create(@RequestBody User user) 
      throws HttpErrorException, JSONParsingException {
    User result = this.restClientService.create(user.getName(), user.getLastname(), user.getProfesion(), user.getEmail());
    Gson gson = new Gson();
    return gson.toJson(result);
  }
  
  /**
   * Este metodo se utiliza para actualizar un usuario. La data se gestiona en formato JSON encode.
   * 
   * @author acalvoa
   * 
   * @param {String} id       El id del usuario que se desea actualizar.
   * @param {User} user       El objeto usuario con los datos proveidos por el JSON en la llamada.
   * 
   * @return Devuelve el usuario creado, en formato JSON.
   */
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void update(@PathVariable String id, @RequestBody User user)
      throws HttpErrorException, JSONParsingException {
    this.restClientService.update(id, user.getName(), user.getLastname(), user.getProfesion(), user.getEmail());
  }
  
   /**
   * Este metodo se utiliza para eliminar un usuario. La data se gestiona en formato JSON encode.
   * 
   * @author acalvoa
   * 
   * @param {String} id       El id del usuario que se desea eliminar.
   * 
   * @return Devuelve el usuario creado, en formato JSON.
   */
  @DeleteMapping("/{id}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void delete(@PathVariable String id)
      throws HttpErrorException, JSONParsingException {
    this.restClientService.delete(id);
  }

  /**
   * Este metodo se utiliza para efectuar un handler de error que devulve el codigo de error HTTP
   * 
   * @author acalvoa
   * 
   * @param {JSONParsingException} error  Error que contiene información del error gatillado.
   * 
   * @return Devuelve el codigo de error como respuesta.
   */
  @ExceptionHandler({ HttpErrorException.class })
  public ResponseEntity<String> handleHttpException(HttpErrorException ex) {
    return ResponseEntity.status(HttpStatus.valueOf(ex.getCode())).body("");
  }
  /**
   * Este metodo se utiliza para efectuar un handler de error que devulve el codigo de error HTTP
   * 
   * @author acalvoa
   * 
   * @param {JSONParsingException} error  Error que contiene información del error gatillado.
   * 
   * @return Devuelve el codigo de error como respuesta.
   */
  @ExceptionHandler({ JSONParsingException.class })
  public ResponseEntity<String> handleJSONException() {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("");
  }
}
