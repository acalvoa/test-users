package com.cleverit.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cleverit.exceptions.HttpErrorException;
import com.cleverit.exceptions.JSONParsingException;
import com.cleverit.models.User;
import com.cleverit.services.UserService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/")
public class UserController {
	@Autowired
	private UserService restClientService;
  
  /**
   * Este metodo se utiliza como bootstrap de la aplicación y carga la app react con los datos iniciales
   * directamente en su semilla. Puede funcionar sin necesidad de AJAX
   * 
   * @author acalvoa
   * 
   * @param {Map} model         Parametro inyectado utilizado para cargar los datos del modelo.
   * 
   * @return Devuelve una vista que es renderizada en el cliente. En este caso carga la aplicación
   * React con los datos de usuarios actualizados
   */
	@GetMapping
	public ModelAndView fetch(Map<String, Object> model) 
	    throws HttpErrorException, JSONParsingException {
	  List<User> users = this.restClientService.fetch();
	  Gson gson = new Gson();
	  model.put("content", gson.toJson(users));
	  return new ModelAndView("index", model);
	}
  
  /**
   * Este metodo se utiliza para crear un usuario mediante llamada POST convencional (Form-urlencode)
   * Una vez crea el usuario el metodo retorna la lista actualizada con los usuarios para que al recargar
   * la pagina la semilla se actualice con los nuevos datos
   * 
   * @author acalvoa
   * 
   * @param {Map} model         Parametro inyectado utilizado para cargar los datos del modelo.
   * @param {String} name       Parametro que contiene el nombre del usuario a crear
   * @param {String} lastname   Parametro que contiene el apellido del usuario a crear
   * @param {String} profesion  Parametro que contiene la profesion del usuario a crear.
   * @param {String} email      Parametro que contiene el email del usuario a crear.
   * 
   * @return Devuelve una vista que es renderizada en el cliente. En este caso carga la aplicación
   * React con los datos de usuarios actualizados
   */
	@PostMapping
	public ModelAndView create(Map<String, Object> model, 
	    String name, String lastname, String profesion, String email) 
	        throws HttpErrorException, JSONParsingException {
    List<User> users = this.restClientService.createAndFetch(name, lastname, profesion, email);
    Gson gson = new Gson();
    model.put("content", gson.toJson(users));
    return new ModelAndView("index", model);
	}
  
  /**
   * Este metodo se utiliza para actualizar un usuario mediante llamada POST convencional (Form-urlencode)
   * Una vez crea el usuario el metodo retorna la lista actualizada con los usuarios para que al recargar
   * la pagina la semilla se actualice con los nuevos datos
   * 
   * @author acalvoa
   * 
   * @param {Map} model         Parametro inyectado utilizado para cargar los datos del modelo.
   * @param {String} id         Parametro que contiene el id del usuario a actualizar
   * @param {String} name       Parametro que contiene el nombre del usuario a actualizar
   * @param {String} lastname   Parametro que contiene el apellido del usuario a actualizar
   * @param {String} profesion  Parametro que contiene la profesion del usuario a actualizar.
   * @param {String} email      Parametro que contiene el email del usuario a actualizar.
   * 
   * @return Devuelve una vista que es renderizada en el cliente. En este caso carga la aplicación
   * React con los datos de usuarios actualizados
   */
	@PostMapping("/update/{id}")
  public ModelAndView update(Map<String, Object> model, @PathVariable String id, 
      String name, String lastname, String profesion, String email)
      throws HttpErrorException, JSONParsingException {
    List<User> users = this.restClientService.updateAndFetch(id, name, lastname, profesion, email);
    Gson gson = new Gson();
    model.put("content", gson.toJson(users));
    return new ModelAndView("index", model);
  }
  
  /**
   * Este metodo se utiliza para eliminar un usuario mediante llamada GET convencional (Form-urlencode)
   * Una vez crea el usuario el metodo retorna la lista actualizada con los usuarios para que al recargar
   * la pagina la semilla se actualice con los nuevos datos
   * 
   * @author acalvoa
   * 
   * @param {Map} model         Parametro inyectado utilizado para cargar los datos del modelo.
   * @param {String} id         Parametro que contiene el id del usuario a actualizar.
   * 
   * @return Devuelve una vista que es renderizada en el cliente. En este caso carga la aplicación
   * React con los datos de usuarios actualizados
   */
	@GetMapping("/destroy/{id}")
  public ModelAndView delete(Map<String, Object> model, @PathVariable String id)
      throws HttpErrorException, JSONParsingException {
	  List<User> users = this.restClientService.deleteAndFetch(id);
    Gson gson = new Gson();
    model.put("content", gson.toJson(users));
    return new ModelAndView("index", model);
  }

  /**
   * Este metodo se utiliza para efectuar un handler de error sobre la app REACT. En este caso devuelve
   * una app sin datos indicando el origen del error.
   * 
   * @author acalvoa
   * 
   * @param {HttpErrorException} error  Error que contiene información del error gatillado.
   * 
   * @return Devuelve una vista que es renderizada en el cliente. En este caso carga la aplicación
   * React sin datos, haciendo uso del handler de error.
   */
  @ExceptionHandler({ HttpErrorException.class })
  public ModelAndView handleHttpException(HttpErrorException ex) {
    HashMap<String, Object> model = new HashMap<String, Object>();
    model.put("content", "[]");
    model.put("error", ex.getCode());
    return new ModelAndView("index", model);
  }
  
  /**
   * Este metodo se utiliza para efectuar un handler de error sobre la app REACT. En este caso devuelve
   * una app sin datos indicando el origen del error.
   * 
   * @author acalvoa
   * 
   * @param {JSONParsingException} error  Error que contiene información del error gatillado.
   * 
   * @return Devuelve una vista que es renderizada en el cliente. En este caso carga la aplicación
   * React sin datos, haciendo uso del handler de error.
   */
  @ExceptionHandler({ JSONParsingException.class })
  public ModelAndView handleJSONException() {
    HashMap<String, Object> model = new HashMap<String, Object>();
    model.put("content", "[]");
    model.put("error", "JSON_PARSING");
    return new ModelAndView("index", model);
  }
	
}
