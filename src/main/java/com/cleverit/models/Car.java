package com.cleverit.models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.google.api.client.util.Key;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Car  {
  @Key
  @Id
  private String id;
  @Key("patente")
  private String plate;
  @Key("tipoAuto")
  private String type;
  @Key("color")
  private String color;
}
