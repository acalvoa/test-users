package com.cleverit.models;

import com.google.api.client.util.Key;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
  @Key
	private String id;
  @Key("nombre")
	private String name;
  @Key("apellido")
	private String lastname;
  @Key("profesion")
	private String profesion;
  @Key("email")
	private String email;
  
  public JsonObject toJSON() {
    JsonObject gson = new JsonObject();
    gson.addProperty("nombre", this.name);
    gson.addProperty("apellido", this.lastname);
    gson.addProperty("profesion", this.profesion);
    gson.addProperty("email", this.email);
    return gson;
  }
}
