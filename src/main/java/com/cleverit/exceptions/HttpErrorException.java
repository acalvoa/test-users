package com.cleverit.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class HttpErrorException extends Exception {

  private static final long serialVersionUID = 1L;
  private int code;
}
