import usersReducers from './user.reducers';
import appReducers from './app.reducers';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
  users: usersReducers,
  app: appReducers
});

export default allReducers;