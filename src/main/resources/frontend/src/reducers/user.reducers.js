import { EDIT_USER, SHOW_EDITOR, UPDATE_USERS, CREATE_USER } from '../actions/do-users';

const initialState = {
  user: null,
  show: false,
  create: false,
  data: window.appData
};

// Generate the reducer with the actions
export default function(state = initialState, action) {
  switch (action.type) {
    case EDIT_USER:
      return Object.assign({}, state, { ...action.payload });
    case CREATE_USER:
      return Object.assign({}, state, { create: action.payload });
    case SHOW_EDITOR:
      return Object.assign({}, state, { ...action.payload });
    case UPDATE_USERS:
      return Object.assign({}, state, { ...action.payload });
    default:
      return state;
  }
}
