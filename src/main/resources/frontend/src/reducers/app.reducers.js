export const LOADING_CHANGE = 'LOADING_CHANGE';

const initialState = {
  loading: false
};

// Generate the reducer with the actions
export default function(state = initialState, action) {
  switch (action.type) {
    case LOADING_CHANGE:
      return Object.assign({}, state, { loading: action.payload });
    default:
      return state;
  }
}
