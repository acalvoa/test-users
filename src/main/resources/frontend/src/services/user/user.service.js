import RestService from '../rest/rest.service';
import { Observable } from 'rxjs';

class UserService {

  static instance;

  static getInstance = () => {
    if (!UserService.instance) {
      UserService.instance = new UserService;
    }
    return UserService.instance;
  }

  static fetchUsers = () => {
    return new Observable(observe => {
      RestService.get('users').subscribe(data => {
        observe.next(data);
        observe.complete();
      })
    });
  }

  static createUser = (user) => {
    return new Observable(observe => {
      RestService.post(`users`, user).subscribe(data => {
        observe.next(data);
        observe.complete();
      })
    });
  }

  static editUser = (user) => {
    return new Observable(observe => {
      RestService.put(`users/${user.id}`, user).subscribe(data => {
        observe.next(data);
        observe.complete();
      })
    });
  }

  static deleteUser = (user) => {
    return new Observable(observe => {
      RestService.delete(`users/${user.id}`).subscribe(data => {
        observe.next(data);
        observe.complete();
      })
    });
  }
}

export default UserService;