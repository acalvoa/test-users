import axios from 'axios';
import { Observable } from 'rxjs';
import { Config } from '../../config/config';

class RestService {
  static instance;

  static getInstance = () => {
    if (!RestService.instance) {
      RestService.instance = new RestService();
    }
    return RestService.instance;
  }

  static get(uri) {
    return RestService.getInstance().get(uri);
  }

  get = (uri) => {
    return new Observable(observe => {
      axios.get(`${Config.API}/${uri}`, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static post = (uri, data) => {
    return RestService.getInstance().post(uri, data);
  }

  post = (uri, data) => {
    return new Observable(observe => {
      axios.post(`${Config.API}/${uri}`, data, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static put = (uri, data) => {
    return RestService.getInstance().put(uri, data);
  }

  put = (uri, data) => {
    return new Observable(observe => {
      axios.put(`${Config.API}/${uri}`, data, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static delete = (uri) => {
    return RestService.getInstance().delete(uri);
  }

  delete = (uri) => {
    return new Observable(observe => {
      axios.delete(`${Config.API}/${uri}`, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  static patch = (uri, data) => {
    return RestService.getInstance().patch(uri, data);
  }

  patch = (uri, data) => {
    return new Observable(observe => {
      axios.patch(`${Config.API}/${uri}`, data, {
        headers: this.getHeaders()
      }).then(data => {
        observe.next(data);
        observe.complete();
      }).catch(error => {
        observe.error(error);
        observe.complete();
      });
    });
  }

  getHeaders = () => {
    let headers = {
      "Content-Type": "application/json"
    };
    return headers;
  }
}

export default RestService;