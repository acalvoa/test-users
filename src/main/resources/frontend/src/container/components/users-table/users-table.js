import React from 'react';
import './users-table.scss';
import { Table, Space } from 'antd';
import { showEditor, deleteUser } from '../../../actions/do-users';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

class UsersTable extends React.Component {

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Nombre',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Apellido',
      dataIndex: 'lastname',
      key: 'lastname',
    },
    {
      title: 'Profesion',
      dataIndex: 'profesion',
      key: 'profesion',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          <span className="link" onClick={this.showEditor.bind(this, record)}>Editar</span>
          <span className="link" onClick={this.deleteUser.bind(this, record)}>Eliminar</span>
        </Space>
      ),
    }
  ];

  showEditor(record) {
    this.props.showEditor(true, record);
  }

  deleteUser(record) {
    Modal.confirm({
      title: 'Confirmación',
      icon: <ExclamationCircleOutlined />,
      content: `Esta seguro que desea eliminar a ${record.name} ${record.lastname}`,
      okText: 'Eliminar',
      cancelText: 'Cancelar',
      onOk: () => this.props.deleteUser(record)
    });
  }

  render() {
    return (
      <div class="container__users">
        <Table columns={this.columns} dataSource={this.props.users} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    showEditor,
    deleteUser
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersTable);
