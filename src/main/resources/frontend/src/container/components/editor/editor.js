import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Drawer } from 'antd';
import { editUser, showEditor } from '../../../actions/do-users';
import './editor.scss';

class Editor extends React.Component {

  state = {
    name: null,
    lastname: null,
    profesion: null,
    email: null,
    show: false
  }

  setName = (options, event) => {
    this.setState({
      name: event.target.value
    });
  }

  setLastname = (options, event) => {
    this.setState({
      lastname: event.target.value
    });
  }

  setProfesion = (options, event) => {
    this.setState({
      profesion: event.target.value
    });
  }
  
  setEmail = (options, event) => {
    this.setState({
      email: event.target.value
    });
  }

  edit = () => {
    const user = {
      id: this.props.user.id,
      name: this.state.name || this.props.user.name,
      lastname: this.state.lastname || this.props.user.lastname,
      profesion: this.state.profesion || this.props.user.profesion,
      email: this.state.email || this.props.user.email  
    };
    this.props.editUser(user);
  }

  onClose = () => {
    this.props.showEditor(false, null);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.show !== prevState.show) {
      return {
        name: nextProps.user ? nextProps.user.name : '',
        lastname: nextProps.user ? nextProps.user.lastname : '',
        profesion: nextProps.user ? nextProps.user.profesion : '',
        email: nextProps.user ? nextProps.user.email : '',
        show: nextProps.show
      };
    }
  }

  render = () => {
    return (
      <>
      <Drawer
        title="Editar Usuario"
        placement="right"
        closable={true}
        width="400"
        onClose={this.onClose}
        visible={this.props.show && this.props.user}
      >
        <form class="create__form">
          <input type="text" 
                placeholder="Nombre"
                name="name"
                value={this.state.name} 
                onChange={this.setName.bind(this, {})}>
          </input>

          <input type="text" 
                placeholder="Apellido" 
                name="lastname" 
                value={this.state.lastname}
                onChange={this.setLastname.bind(this, {})}>
          </input>

          <input type="text" 
                placeholder="Profesión"
                name="profesion" 
                value={this.state.profesion}
                onChange={this.setProfesion.bind(this, {})}>
          </input>

          <input type="email" 
                placeholder="Email"
                name="email" 
                value={this.state.email}
                onChange={this.setEmail.bind(this, {})}>
          </input>
          <button type="button" 
                value="Editar" 
                class="create__form__action"
                onClick={this.edit.bind(this)}>Editar Usuario
          </button>
        </form>
      </Drawer>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    show: state.users ? state.users.show : false,
    user: state.users ? state.users.user : null
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    editUser,
    showEditor
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
