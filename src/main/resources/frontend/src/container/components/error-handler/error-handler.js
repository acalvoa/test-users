import React from 'react';
import './error-handler.scss';
import Error from '../../../assets/error.png';

const NOT_FOUND = "404";
const SERVER_ERROR = "404";
const JSON_PARSING_ERROR = "JSON_PARSING";
const TIME_OUT = "408";

class ErrorHandler extends React.Component {

  printError = () => {
    switch (this.props.error) {
      case NOT_FOUND:
        return 'El Registro no esta por ninguna parte. Prueba con otro registro.';
      case SERVER_ERROR:
        return 'Esta vez hicimos implosión. Avisa al administrador.';
      case JSON_PARSING_ERROR:
        return 'No logramos entender el contenido del mensaje.';
      case TIME_OUT:
        return 'La fuente de datos no responde. Prueba en unos minutos.';
      default:
        return `Tenemos un error desconocido. El codido es: ${this.props.error}`;
    }
  }

  render = () => {
    return (
      <div class="container__error">
        <div class="image"><img src={Error} alt="error" /></div>
        <div class="message">{this.printError()}</div>
      </div>
    );
  }
}

export default ErrorHandler;
