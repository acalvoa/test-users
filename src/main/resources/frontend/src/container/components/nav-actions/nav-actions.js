import React from 'react';
import './nav-actions.scss';
import { Drawer } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createUser, showEditor } from '../../../actions/do-users';

class NavActions extends React.Component {

  state = {
    name: null,
    lastname: null,
    profesion: null,
    email: null
  }

  setName = (options, event) => {
    this.setState({
      name: event.target.value
    });
  }

  setLastname = (options, event) => {
    this.setState({
      lastname: event.target.value
    });
  }

  setProfesion = (options, event) => {
    this.setState({
      profesion: event.target.value
    });
  }
  
  setEmail = (options, event) => {
    this.setState({
      email: event.target.value
    });
  }

  create = () => {
    this.props.showEditor(true, null, true);
  }

  onClose = () => {
    this.props.showEditor(false, null, true);
  }

  doCreate = () => {
    const user = {
      name: this.state.name,
      lastname: this.state.lastname,
      profesion: this.state.profesion,
      email: this.state.email
    };
    this.props.createUser(user);
  }

  render() {
    return (
      <>
      <div class="container__nav" onClick={this.create}>+</div>
      <Drawer
        title="Crear Usuario"
        placement="right"
        closable={false}
        width="400"
        onClose={this.onClose}
        visible={this.props.create}
      >
        <form class="create__form">
          <input type="text" 
                placeholder="Nombre"
                name="name" 
                onChange={this.setName.bind(this, {})}>
          </input>

          <input type="text" 
                placeholder="Nombre" 
                name="lastname" 
                onChange={this.setLastname.bind(this, {})}>
          </input>

          <input type="text" 
                placeholder="Profesión"
                name="profesion" 
                onChange={this.setProfesion.bind(this, {})}>
          </input>

          <input type="email" 
                placeholder="Email"
                name="email"
                onChange={this.setEmail.bind(this, {})}>
          </input>
          <button type="button" 
                class="create__form__action"
                onClick={this.doCreate.bind(this)}>Crear Usuario
          </button>
        </form>
      </Drawer>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    create: state.users ? state.users.create : false
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    createUser,
    showEditor
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NavActions);
