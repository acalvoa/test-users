import React from 'react';
import './header.scss';
import logo from '../../../assets/logo.png'

class Header extends React.Component {

  render = () => {
    return (
      <div class="container__header">
        <div class="logo"><img src={logo} alt="CleverIT"></img></div>
        <div class="title">Mantenedor de usuarios</div>
      </div>
    );
  }
}

export default Header;
