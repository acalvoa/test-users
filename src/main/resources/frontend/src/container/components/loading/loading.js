import React from 'react';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import './loading.scss';

class Loading extends React.Component {
  render() {
    if (this.props.loading) {
      return (
        <div class="container__loading">
          <Spin size="large" />
        </div>
      );
    } else {
      return (<></>);
    }
  }
}

const mapStateToProps = state => {
  return {
    loading: state.app ? state.app.loading : false
  }
}

export default connect(mapStateToProps)(Loading);
