import React from 'react';
import { connect } from 'react-redux';
import Header from './components/header/header';
import UsersTable from './components/users-table/users-table';
import NavActions from './components/nav-actions/nav-actions';
import Editor from './components/editor/editor';
import ErrorHandler from './components/error-handler/error-handler';
import Loading from './components/loading/loading';
import './app.scss';

class App extends React.Component {

  hasError = () => {
    if (this.props.error) {
      return (<ErrorHandler error={this.props.error}></ErrorHandler>);
    } else {
      return (
        <>
        <UsersTable users={this.props.data}></UsersTable>
        <NavActions></NavActions>
        <Editor></Editor>
        </>
      );
    }
  }

  render = () => {
    return (
      <div class="container">
        <Loading></Loading>
        <Header></Header>
        {this.hasError()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.users.data
  }
}

export default connect(mapStateToProps)(App);
