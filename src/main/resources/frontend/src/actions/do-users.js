import UserService from '../services/user/user.service';
import { LOADING_CHANGE } from '../reducers/app.reducers';

export const SHOW_EDITOR = "SHOW_EDITOR";
export const UPDATE_USERS = "UPDATE_USERS";
export const CREATE_USER = "CREATE_USER";
export const EDIT_USER = "EDIT_USER";
export const DELETE_USER = "DELETE_USER";


export const editUser = (user) => {
  return (dispatch, getState) => {
    dispatch({
      payload: true,
      type: LOADING_CHANGE
    });
    UserService.editUser(user).subscribe(data => {
      UserService.fetchUsers().subscribe(users => {
        dispatch({
          payload: {
            user: user,
            show: false
          },
          type: SHOW_EDITOR
        });
        dispatch({
          payload: {
            data: users.data
          },
          type: UPDATE_USERS
        });
        dispatch({
          payload: false,
          type: LOADING_CHANGE
        });
      });
    });
  }
};

export const deleteUser = (user) => {
  return (dispatch, getState) => {
    dispatch({
      payload: true,
      type: LOADING_CHANGE
    });
    UserService.deleteUser(user).subscribe(data => {
      UserService.fetchUsers().subscribe(users => {
        dispatch({
          payload: {
            data: users.data
          },
          type: UPDATE_USERS
        });
        dispatch({
          payload: false,
          type: LOADING_CHANGE
        });
      });
    })  
  }
};

export const createUser = (user) => {
  return (dispatch, getState) => {
    dispatch({
      payload: true,
      type: LOADING_CHANGE
    });
    UserService.createUser(user).subscribe(data => {
      UserService.fetchUsers().subscribe(users => {
        dispatch({
          payload: false,
          type: CREATE_USER
        });
        dispatch({
          payload: {
            data: users.data
          },
          type: UPDATE_USERS
        });
        dispatch({
          payload: false,
          type: LOADING_CHANGE
        });
      });
    });
  }
};

export const showEditor = (show, user, create) => {
  return (dispatch, getState) => {
    if (create) {
      dispatch({
        payload: show,
        type: CREATE_USER
      });
    } else {
      dispatch({
        payload: {
          user: user,
          show: show
        },
        type: SHOW_EDITOR
      });
    }
  }
};